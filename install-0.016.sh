#! /bin/bash

# INSTALLATIONSSKRIPT ZUR NEUINSTALLATION VON TRYTON

# Änderungen
# separate Liste für pypi wieder rausgenommen - wird im Update-Skript über die Versionsnummer gehandhabt.


killall trytond
killall trytond-cron

# set -e # Ende nach 1.Fehler
# set -x # Fehlermeldungen ausgeben

instskrverz=$(pwd) # instskrverz ist das Verzeichnis, in denen das Installationsskript liegt
datum=`date +%d.%m.%Y`
cd ~

echo "
Wir gehen davon aus, daß die virtuellen Umgebungen für Ihre Tryton-Versionen ~/TRYTON liegen sollen oder bereits liegen. In diesem Fall: Eingabetaste.

Sofern Sie einen anderen Pfad in Ihrem Heimatverzeichnis wünschen, geben Sie diesen bitte OHNE "/home/ihr-Benutzername/" ein.
"
read Pfadkennung
if [ "$Pfadkennung" = "" ]
then
mkdir ~/TRYTON
pfadtry=~/TRYTON
else
mkdir ~/$Pfadkennung
pfadtry=~/$Pfadkennung
fi

cd $pfadtry

echo "
Möchten Sie die aktuellste Tryton-Version installieren - oder eine andere? (j / Versionsnummer x.y.*)
"
read Zielversionsnummer


if [ "$Zielversionsnummer" = "j" ]
then
Zielversionsnummer= # Zielversionsnummer ist leer
else
Zielversionsnummer===$Zielversionsnummer # Zielversionsnummer enthält "==" zur Weiterverarbeitung
fi

echo "
Sollen Verzeichnis und Datenbank den gleichen Namen haben (j/n)?
"
read gl_Name

if [ "$gl_Name" = "n" ]
then
echo "
Wie soll das neue Verzeichnis für die Installation der ersten virtuellen Umgebung für Tryton heißen?
"
read neues_Verzeichnis

echo "
Wie soll die neue Datenbank heißen?
"
read neue_db_Version

else

echo "
Wie sollen das neue Verzeichnis und die neue Datenbank heißen?
"
read neues_Verzeichnis
neue_db_Version=$neues_Verzeichnis

fi

mkdir $pfadtry/$neues_Verzeichnis
cd $pfadtry/$neues_Verzeichnis

echo "
Bitte geben sie den gewünschten Namen des Datenbank-Benutzers ein:"
read db_Benutzer

echo "
Bitte geben Sie das bzw. ein neues Kennwort für Ihren Datenbank-Benutzer $db_Benutzer ein:"
read kw_dbBenutzer


### Distro-spezifische Installationen ###

Distribution=$(lsb_release -is)
if [ "$Distribution" = "Ubuntu" ] || [ "$Distribution" = "Debian" ]
then 
echo Ubuntu oder Debian 
sudo apt-get install python3-venv python3-setuptools python3-gi-cairo python3-gi-cairo gir1.2-goocanvas-2.0 postgresql
fi

### Kitalux Sonderabfrage: GTK2 und dateutil installieren. ###
if [ "$Zielversionsnummer" = "==4.6.*" ]
then
wget http://archive.ubuntu.com/ubuntu/pool/universe/p/pygtk/python-gtk2_2.24.0-5.1ubuntu2_amd64.deb
sudo apt-get install ./python-gtk2_2.24.0-5.1ubuntu2_amd64.deb
sudo apt-get install python-dateutil python-chardet
rm python-gtk2_2.24.0-5.1ubuntu2_amd64.deb
fi
### Ende Kitalux Sonderabfrage: GTK2 und dateutil installieren. ###


if [ "$Distribution" = "ManjaroLinux" ] || [ "$Distribution" = "ArchLinux" ]
then
echo Manjaro oder Arch
sudo pacman -S --needed python python3-venv python3-setuptools python3-gi-cairo python3-gi-cairo gir1.2-goocanvas-2.0 goocanvas postgresql
fi

if [ "$Distribution" = "RedHat" ]
then
echo RedHat
fi

### Ende Distro-spezifische Installationen ###


sudo systemctl start postgresql
sudo systemctl enable --now postgresql.service


### PIP und Tryton-Basis-Pakete installieren ###
python -m venv --system-site-packages $pfadtry/$neues_Verzeichnis
cd $pfadtry/$neues_Verzeichnis
. bin/activate
pip install --upgrade pip
pip install psycopg2-binary Genshi lxml passlib pycountry forex_python polib proteus python-magic python-sql relatorio simplejson Werkzeug==2.2.3 wrapt 
pip install trytond$Zielversionsnummer proteus$Zielversionsnummer

### Ende PIP und Tryton-Basis-Pakete installieren ###


### Modulliste öffentliche Module 
echo "
Sie können jetzt die Auswahl der Module über Modull bestimmen. Sie können 3 Listen verwenden:

1. *.lst - eine Liste der Tryton-spezifischen Module mit der Versionsnummer Ihrer Tryton-installation. Bitte kopieren Sie eine davon ins Verzeichnis $neues_Verzeichnis. Wenn Sie möchten, können Sie Ihre Modulliste nach Ihren Wünschen abändern.

2. *.lstprpy - zusätzliche Module aus einem privaten pypi-Repo. Für diesen Fall braucht es noch eine Datei mit Benutzerdaten, bitte legen Sie eine Datei mit der Bezeichnung "Benutzerdaten.prpy" sowie eine Liste der Module mit der Endung ".lstprpy" ins Verzeichnis $neues_Verzeichnis. 
(Wird normalerweise nicht gebraucht.)

Wenn Sie eine vorkonfigurierte Datenbank verwenden möchten, wählen Sie eine Modulliste, deren Bezeichnung mit der einer Musterdatenbank korrespondiert. Bitte verändern Sie in diesem Falle die Modulliste nicht. (Weiter mit Eingabetaste)
"

read

cd $pfadtry/$neues_Verzeichnis

## gegebenenfalls Versionsnummer aus vorherigem Installationsversuch entfernen:
sed -i "s/==.*$//g" *.lst 
# Zielversionsnummer einfügen:
sed -i "s/$/$Zielversionsnummer/g" *.lst 

### Tryton-Kernmodule installieren ###
pip install -r *.lst

if [ "$?" <> "0" ]
then
echo Die Installation der Kernmodule ist gescheitert oder unvollständig - bitte Korrektur auf anderem Terminal vornehmen! - Weiter mit Eingabe.
read
fi
### Ende Tryton-Kernmodule installieren ###

### Privates Pypi  ###
IFS=";"
read -a BenDaten < *.prpy
BenPrpy=${BenDaten[0]}
KennwPrpy=${BenDaten[1]}
ServerPrpy=${BenDaten[2]}
# Module aus privaten Pypi installieren:
pip install --no-dependencies --no-index --find-links https://$BenPrpy:$KennwPrpy@$ServerPrpy -r *.lstprpy
if [ "$?" <> "0" ]
then
echo Die Installation von Modulen aus einem privaten PyPi ist gescheitert oder unvollständig - bitte Korrektur auf anderem Terminal vornehmen! - Weiter mit Eingabe.
read
fi
### Ende privates Pypi ###

echo "
Installieren Sie gegebenenfalls manuell zu installierende Pakete - zum Beispiel von  github oder aus .whl-Dateien - in einem separaten Terminal-Fenster. Die entsprechenden Befehle lauten normalerweise

cd $pfadtry/$neues_Verzeichnis
. bin/activate
pip install --no-dependencies [Name-desPakets].whl

(Eingabetaste, wenn fertig.)
"
read

sudo -u postgres psql --command="CREATE ROLE $db_Benutzer WITH PASSWORD '$kw_dbBenutzer';"
sudo -u postgres psql --command="ALTER ROLE $db_Benutzer WITH LOGIN;"
sudo -u postgres dropdb $neue_db_Version
sudo -u postgres createdb -O $db_Benutzer $neue_db_Version

echo "
Haben Sie bereits den *.sql-Export (oder eine Musterdatenbank) einer Tryton-Datenbank, den Sie importieren möchten? Diese Datenbank muß mit dem Argument --no-user exportiert worden sein. (j/n)
"
read db_vorh
if [ "$db_vorh" = "j" ]
then
echo "
Dann kopieren Sie die Datei bitte in das Verzeichnis $pfadtry/$neues_Verzeichnis und geben ihm die Endung ".sql", falls noch nicht vorhanden. (Eingabetaste, wenn fertig).
"
read

cd $pfadtry/$neues_Verzeichnis

cp *.sql Arbeitsdatei.sql

# sed -i "s/wolf/$db_Benutzer/g" Arbeitsdatei.sql # Benutzername im sql-dump erstetzen

sudo -u postgres psql -d $neue_db_Version -U $db_Benutzer < Arbeitsdatei.sql
# psql -U $db_Benutzer -d $neue_db_Version < $pfadtry/$neues_Verzeichnis/Arbeitsdatei.sql
fi

echo "[database]
uri = postgresql://$db_Benutzer:$kw_dbBenutzer@localhost:5432/

language = de

[product]
price_decimal = 6

"  > $pfadtry/$neues_Verzeichnis/trytond.conf 

### Datenbank in venv registrieren ###
echo Der folgende Schritt kann einige Minuten dauern.
trytond-admin -c trytond.conf -d $neue_db_Version --all -vv
### Ende Datenbank in venv registrieren ###


echo "
Wenn Sie eine weitere Sprache außer Englisch wünschen, geben Sie bitte deren beiden Kennbuchstaben (fr, es, de, it etc.) ein.
"
read sprache
trytond-admin -c trytond.conf -d $neue_db_Version -l $sprache

### Länderliste ###
echo "
Möchten Sie eine Liste aller Länder weltweit importieren (j/n)?
"
read janein

if [ "$janein" = "j" ]
then
trytond_import_countries -d $neue_db_Version -c trytond.conf
fi
### Ende Länderliste ###

### Währungen ###
echo "
Möchten Sie eine Liste aller Weltwährungen importieren (j/n)?
"
read janein

if [ "$janein" = "j" ]
then
trytond_import_currencies -d $neue_db_Version -c trytond.conf
fi
### Ende Währungen ###

### PLZs ###
echo "
Möchten Sie eine Liste aller Postleitzahlen Ihres Landes importieren - dauert mehrere Minuten (j/n)?
"
read janein

if [ "$janein" = "j" ]
then
trytond_import_postal_codes -d $neue_db_Version -c trytond.conf $sprache
fi
### Ende PLZs ###


echo  "
Starten Sie den trytond-Dämon durch Eingabe des Befehls
$ trytond -c trytond.conf -d $neue_db_Version 
im Verzeichnis $pfadtry/$neues_Verzeichnis.
in einem neuen Terminal.
"

### Klient laden ###
echo "
Möchten Sie Ihren Klienten herunterladen (j/n)?
"
read janein

if [ "$janein" = "j" ]
then
xdg-open https://downloads.tryton.org
fi
### Ende Klient laden ###

echo "
Enpacken Sie den Klienten, wechseln Sie in das betreffende Verzeichnis und installieren Sie ihn mit
sudo python3 setup.py install
"

if [ "$Zielversionsnummer" = "==4.6.*" ]
then
echo "
Im Klienten-Paket ändern Sie bitte die erste Zeile in tryton/bin in:
#!/usr/bin/env python2
"
fi

echo "
Falls Sie neben Englisch eine weitere Sprache installiert haben, müssen Sie diese in der graphischen Benutzeroberfläche aktivieren:

Administration > Localisazion > Languages ==> load translation.

Außerdem müssen sie die installierten Module aktivieren:
Administration > Modules > Modules).
"

### Startskript erzeugen ###
echo "
Möchten Sie eine Startskript für Ihre Installation erzeugen (j/n)?
"
read janein

if [ "$janein" = "j" ]
then

echo "#! /bin/bash
killall trytond
killall trytond-cron
cd $TryPfad/$neues_Verzeichnis
. bin/activate
sleep .5
trytond-cron -c trytond.conf -d $neues_Verzeichnis &
trytond -c trytond.conf -d $neues_Verzeichnis &
sleep 3
tryton" > ~/$TryPfad/$neues_Verzeichnis/Startskript-$neues_Verzeichnis.sh

echo "
Das Startskript liegt im Verzeichnis $TryPfad/$neues_Verzeichnis unter dem Namen 'Startskript-$neues_Verzeichnis.sh' für Sie bereit.
"

fi
### Ende Startskript erzeugen ###

echo Ende des Skripts - wir wünschen noch einen schönen Tag."
