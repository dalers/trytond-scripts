#!/bin/bash

# In den Zeilen 11-14 sowie 19 sind Benutzereingaben notwendig!

sleep 60
killall trytond
# set -x
alteNr=1
neueNr=$(( $alteNr+1 ))

### Hier Benutzereingaben: ###
sikopPfad=  # Pfad der Sicherungskopien 
dbName= # Name Datenbank 
dbBenutzer= # Datenbank-Benutzername 
skriptPfad= # Pfad dies Skripts 
nameSkript=Tryton-Sicherungsskript.sh # Name dieses Skripts
db_Kennwort= # Datenbank-Kennwort 
### Ende Benutzereingaben. ###

WoTag=`date +%a`
neueNr=$(( $alteNr+1 ))
datum=`date +%d.%m.%Y`
nameSikopNeu=$dbName--$datum-$WoTag--$neueNr.sql


killall trytond

PGPASSWORD=$db_Kennwort pg_dump -U $dbBenutzer $dbName > $sikopPfad/$nameSikopNeu

echo $sikopPfad/$dbName*--$neueNr.sql $sikopPfad/$dbName*--$alteNr.sql

cmp $sikopPfad/$dbName*--$neueNr.sql $sikopPfad/$dbName*--$alteNr.sql
 if [ $? == 0 ]; then
	rm $sikopPfad/$dbName*--$neueNr.sql
	neueNr=$(( $neueNr-1 ))
	echo gleich
  else
	gzip $sikopPfad/$dbName*--$alteNr.sql 
	echo verschieden
	sed -i "5i alteNr=${neueNr}" $skriptPfad/$nameSkript
	sed -i '6d' $skriptPfad/$nameSkript
 fi

