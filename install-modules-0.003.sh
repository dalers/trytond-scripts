#! /bin/bash

# INSTALLATIONSSKRIPT ZUR INSTALLATION ZUSÄTZLICHER MODULE IN TRYTON

# set -x
instskrverz=$(pwd) # instskrverz ist das Verzeichnis, in denen das Installationsskript liegt

killall trytond
killall trytond-cron
datum=`date +%d.%m.%Y`

echo "
Wir gehen davon aus, daß die virtuellen Umgebungen für Ihre Tryton-Versionen ~/TRYTON liegen sollen oder bereits liegen. In diesem Fall: Eingabetaste.

Sofern Sie einen anderen Pfad in Ihrem Heimatverzeichnis wünschen, geben Sie diesen bitte OHNE "/home/ihr-Benutzername/" ein.
"
read Pfadkennung
if [ "$Pfadkennung" = "" ]
then
mkdir ~/TRYTON
pfadtry=~/TRYTON
else
mkdir ~/$Pfadkennung
pfadtry=~/$Pfadkennung
fi

cd $pfadtry

echo "Haben das Verzeichnis der virtuellen Umgebung und die Datenbank den gleichen Namen (j/n)?"
read gl_Name

if [ "$gl_Name" = "n" ]
then
echo "Wie heißt das Verzeichnis der virtuellen Umgebung?"
read neues_Verzeichnis

echo "Wie heißt die Datenbank?"
read neue_db_Version

else
echo "Wie heißen Verzeichnis und Datenbank?"
read neues_Verzeichnis
neue_db_Version=$neues_Verzeichnis
fi

cd $neues_Verzeichnis
. bin/activate
pip install --upgrade pip

pip freeze --local > installiert_am_$datum.txt


echo "Welche Module möchten Sie installieren? - Geben Sie sie im Format 'trytond-[modul-name]' an, ohne die Anführungszeichen, mit Bindestrichen. Mehrere Module durch ein Leerzeichen trennen. Wenn Sie den Inhalt einer Modulliste installieren möchten, Eingabetaste."
read zuinstallieren

sed  "/$Vers/!d" installiert_am_$datum.txt > Liste_installiert_$datum.lst # entfernt alle Zeilen, die nicht "==Versionsnummer" enthalten
grep trytond== Liste_installiert_$datum.lst > Versionsnummer
sed -i 's/trytond==//' Versionsnummer # -i: Datei wird überschrieben
echo "Ihre  Versionsnummer ist:"
cat Versionsnummer

echo "Welche Version möchten Sie installieren? - Bitte im Fomat 'x.y.*' eingeben; 'a' installiert die aktuelle Version."
read gew_Versionsnummer

if [ "$gew_Versionsnummer" = "a" ]
then
pip install $zuinstallieren
else
echo $zuinstallieren > mod_zu_installieren.lst

sed -i "s/$/==$gew_Versionsnummer/g" mod_zu_installieren.lst
sed -i "s/ /==$gew_Versionsnummer \n/g" mod_zu_installieren.lst

pip install -r mod_zu_installieren.lst
rm mod_zu_installieren.lst

fi

# Benutzerdaten für privates Pypi einlesen:
IFS=";"
read -a BenDaten < *.prpy
BenPrpy=${BenDaten[0]}
KennwPrpy=${BenDaten[1]}
ServerPrpy=${BenDaten[2]}


sed -i "s/$/==$gew_Versionsnummer/g" *.lstprpy
sed -i "s/ /==$gew_Versionsnummer \n/g" *.lstprpy


# Module aus privaten Pypi installieren:
pip install --no-index --find-links https://$BenPrpi:$KennwPrpy@$ServerPrpy -r *.lstprpy
#


pip freeze --local > installiert_am_$datum.lst

ls

echo "Der folgende Schritt kann mehrere Minuten dauern, bitte Geduld! (Weiter mit Eingabetaste)."
read


trytond-admin -c trytond.conf -d $neues_Verzeichnis -u $zuinstallieren -vv

echo "
Möchten Sie den Tryton-Server automatisch starten ?
"
read starten
if [ "$starten" = "j" ]
then
trytond -c trytond.conf -d $neues_Verzeichnis &
trytond-cron -c trytond.conf -d $neues_Verzeichnis &

echo "
Der Tryton-Server wurde gestartet."
fi

echo "

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Bitte loggen Sie sich in den Tryton-Klienten ein und aktivieren die neuen Module unter
Menu > Verwaltung > Module > Module

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"

echo "

Ende des Skripts - wir wünschen noch einen schönen Tag."


