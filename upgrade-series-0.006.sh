#! /bin/bash

# UPDATE-SKRIPT TRYTON FÜR UBUNTU-LINUX

killall trytond
# set -x

datum=`date +%d.%m.%Y`
cd ~

echo "
Wir gehen davon aus, daß die virtuellen Umgebungen für Ihre Tryton-Versionen im Verzeichnis ~/TRYTON liegen. In diesem Fall: Eingabetaste.

Sofern Sie einen anderen Pfad in Ihrem Heimatverzeichnis wünschen, geben Sie diesen bitte OHNE "/home/ihr-Benutzername/" ein.
"
read Pfadkennung
if [ "$Pfadkennung" = "" ]
then
mkdir ~/TRYTON
pfadtry=~/TRYTON
else
mkdir ~/$Pfadkennung
pfadtry=~/$Pfadkennung
fi

cd $pfadtry

echo "Haben Verzeichnis und Datenbank den gleichen Namen haben und soll das so bleiben (j/n)?"
read gl_Name

if [ "$gl_Name" = "n" ]
then

echo "Wie heißt das alte Verzeichnis?"
read altes_Verzeichnis

echo "Welche alte Datenbank-Version möchten Sie sichern?"
read alte_db_Version

echo "Wie soll das neue Verzeichnis heißen?"
read neues_Verzeichnis

echo "Wie soll die neue Datenbank heißen?"
read neue_db_Version

else

echo "Wie heißt das alte Verzeichnis und die alte Datenbank?"
read altes_Verzeichnis
alte_db_Version=$altes_Verzeichnis

echo "Wie sollen das neue Verzeichnis und die neue Datenbank heißen?"
read neues_Verzeichnis
neue_db_Version=$neues_Verzeichnis

fi

mkdir $neues_Verzeichnis

echo "Bitte geben sie den Namen des Datenbank-Benutzers ein:"
read db_Benutzer

echo "
Bitte geben Sie das Kennwort für Ihren Datenbank-Benutzer $db_Benutzer ein:"
read kw_dbBenutzer

PGPASSWORD="$kw_dbBenutzer" pg_dump -U $db_Benutzer -d $alte_db_Version > $neues_Verzeichnis/try_db_Sicherung_$datum.sql

cd $pfadtry/$altes_Verzeichnis
. bin/activate
pip freeze --local > $pfadtry/$neues_Verzeichnis/im_alten_Verz_Installiert_$datum.txt
deactivate

[ -s $pfadtry/$neues_Verzeichnis/im_alten_Verz_Installiert_$datum.txt ] || echo "
Die Installationsliste im neuen Verzeichnis ist leer. Bitte kopieren Sie gegebenenfalls Inhalte aus einer Sicherungskopie hinein. (Weiter mit Eingabetaste)
" & read

# trap "set +x; sleep 5; set -x" DEBUG

cd $pfadtry/$neues_Verzeichnis
sed '/^trytond/!d' im_alten_Verz_Installiert_$datum.txt > Liste_zu_inst_$datum.lst # alle Zeilen raus, außer beginnen mit trytond
grep trytond== Liste_zu_inst_$datum.lst > Versionsnummer
sed -i 's/trytond==//' Versionsnummer # -i: Datei wird überschrieben

echo Ihre Versionsnummer ist; cat Versionsnummer
echo Bitte geben Sie Ihre Zielversionsnummer im Format "x.y.*" ein:
read Zielversionsnummer
sed -i "s/==.*/==$Zielversionsnummer/" Liste_zu_inst_$datum.lst # Schreibe Zielversionsnummer in Liste_zu_inst_.

echo "Sie können jetzt Änderungen an der Datei Liste_zu_inst_$datum.lst im Verzeichnis $pfadtry/$neues_Verzeichnis vornehmen, zum Beispiel Module löschen oder ergänzen.
(Weiter mit Eingabetaste.)"
read 

cp ../$altes_Verzeichnis/trytond.conf .
python3 -m venv --system-site-packages $pfadtry/$neues_Verzeichnis 
cd $pfadtry/$neues_Verzeichnis

/bin/bash -c "$pfadtry/$neues_Verzeichnis"

echo "Installieren Sie gegebenenfalls manuell zu installierende Pakete - zum Beispiel von  github oder aus .whl-Dateien - in einem anderen Terminal-Fenster.
Aktivieren Sie dazu die virtuelle Umgebung:
$ cd $pfadtry/$neues_Verzeichnis
$ . bin/activate 
und installieren Sie mit:
$ pip install --no-deps [Name-des-Pakets.whl]
Die --no-deps-Option vermeidet unfreiwillige Up- oder Downgrades.
(Weiter mit Eingabetaste.)"
read

. bin/activate
pip install --upgrade pip
pip install psycopg2 Genshi lxml passlib pkg-resources polib psycopg2-binary python-magic python-sql relatorio Werkzeug wrapt
pip install -r Liste_zu_inst_$datum.lst

sudo -u postgres dropdb $neues_Verzeichnis # sicherheitshalber bei Fehlversuchen erstellte Datenbank löschen
sudo -u postgres createdb -O $db_Benutzer $neues_Verzeichnis

echo Als Kennwort geben Sie das für Ihren Datenbank-Benutzernamen $db_Benutzer ein.
PGPASSWORD="$kw_dbBenutzer" psql -U $db_Benutzer -d $neue_db_Version < try_db_Sicherung_$datum.sql

echo "Geben Sie den ersten Teil der manuellen Befehle - vor dem Update - ein. Verwenden Sie dazu ein anderes Termial.
(Eingabe wenn fertig, oder falls keine Befehle erforderlich sind.)

Detailinfos zu den manuellen Befehlen finden Sie -jeweils aktuell - unter
https://discuss.tryton.org/c/migration/6

"
read

trytond-admin -c trytond.conf -d $neue_db_Version --all -v

echo "

Geben Sie den zweiten Teil der Befehle - nach dem Update - ein. Verwenden Sie dazu ein anderes Termial.
(Eingabe wenn fertig, oder falls keine Befehle erforderlich sind.)"
read

trytond-admin -c trytond.conf -d $neue_db_Version --all -vv

echo "
Möchten Sie den Tryton-Server starten? (j/n)? 
"
read serverstart
if [ "$serverstart" = "j" ]
then
cd $pfadtry/$neues_Verzeichnis
trytond -c trytond.conf -d $neue_db_Version &
fi

echo"
Glückwunsch. Sie haben das update nach Tryton $neue_db_Version erfolgreich durchgeführt. Der Tryton-Dämon wurde gestartet. Sie können sich über die Tryton-GUI einloggen."


