#!/bin/bash

# Startskript für Tryton
# Es wird erwartet, daß Datenbank und VirtEnv den gleichen Namen haben und der passende Klient installiert ist.

Tryversion=[Ihre-Tryton-Version] # Name Ihrer virtuellen Umgebung
TryPfad=~/TRYTON # Pfad, in der Ihre virtuellen Umgebungen liegt

killall trytond
killall trytond-cron
cd $TryPfad/$Tryversion
. bin/activate
sleep .5
trytond-cron -c trytond.conf -d $Tryversion &
trytond -c trytond.conf -d $Tryversion &
sleep 3
tryton


