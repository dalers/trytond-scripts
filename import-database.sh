#! /bin/bash

echo "
Dieses Skript importiert eine vorhandene Datenbank in eine vorhandene Installation mit gleicher Hauptversionsnummer. (Eingabe)
"
read

# set -x
instskrverz=$(pwd) # instskrverz ist das Verzeichnis, in denen das Installationsskript liegt

killall trytond

datum=`date +%d.%m.%Y`
cd ~

echo "
Wir gehen davon aus, daß die virtuellen Umgebungen für Ihre Tryton-Versionen ~/TRYTON liegen sollen oder bereits liegen. In diesem Fall: Eingabetaste.

Sofern Sie einen anderen Pfad in Ihrem Heimatverzeichnis wünschen, geben Sie diesen bitte OHNE "/home/ihr-Benutzername/" ein. (Eingabe)
"
read Pfadkennung
if [ "$Pfadkennung" = "" ]
then
mkdir ~/TRYTON
pfadtry=~/TRYTON
else
mkdir ~/$Pfadkennung
pfadtry=~/$Pfadkennung
fi

cd $pfadtry

echo "
Wie heißt das Verzeichnis der virtuellen Umgebung, die zu der zu aktualisierenden Datenbank gehört?"
read neues_Verzeichnis
neue_db_Version=$neues_Verzeichnis

echo "
Bitte geben sie den Namen des Datenbank-Benutzers ein:"
read db_Benutzer

cd $pfadtry/$neues_Verzeichnis
. bin/activate

killall trytond trytond-cron tryton

sudo -u postgres dropdb $neue_db_Version
sudo -u postgres psql --command="CREATE ROLE $db_Benutzer WITH PASSWORD '$kw_dbBenutzer';"
sudo -u postgres psql --command="ALTER ROLE $db_Benutzer WITH LOGIN;"
sudo -u postgres createdb -O $db_Benutzer $neue_db_Version

echo "
Legen Sie die vorhandene Datenbank-Sicherung mit der Endung .sql in das Verzeichnis $pfadtry/$neues_Verzeichnis."
read

psql -U $db_Benutzer -d $neue_db_Version < *.sql

echo Der folgende Schritt kann einige Minuten dauern.
trytond-admin -c trytond.conf -d $neue_db_Version --all -vv

echo "
Ende des Skripts - wir wünschen noch einen schönen Tag."
