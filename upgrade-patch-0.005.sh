#! /bin/bash

# UPDATE-SKRIPT TRYTON FÜR UBUNTU-LINUX

killall trytond
# set -x
datum=`date +%d.%m.%Y`
cd ~

echo "
Wir gehen davon aus, daß die virtuellen Umgebungen für Ihre Tryton-Versionen im Verzeichnis ~/TRYTON liegen. In diesem Fall: Eingabetaste.

Sofern die virtuellen Umgebungen einen anderen Pfad liegen, geben Sie diesen bitte OHNE "/home/ihr-Benutzername/" ein.
"
read Pfadkennung
if [ "$Pfadkennung" = "" ]
then
pfadtry=~/TRYTON
else
pfadtry=~/$Pfadkennung
fi

cd $pfadtry

echo "
Wie heißt das Verzeichnis, in dem die zu aktualisierende Installation liegt?
"
read altes_Verzeichnis

cd $pfadtry/$altes_Verzeichnis
. bin/activate
pip freeze --local > $pfadtry/$altes_Verzeichnis/installiert_vor_Update_am_$datum.txt

# trap "set +x; sleep 2; set -x" DEBUG

cd $pfadtry/$altes_Verzeichnis

echo Inst_Liste_anschauen_01
read

grep trytond== installiert_vor_Update_am_$datum.txt > Versionsnummer # trytond==Versionsnummer rausholen
sed -i 's/trytond==//' Versionsnummer # -i: Datei wird überschrieben, # trytond== entfernen
echo Ihre bisherige Versionsnummer ist; cat Versionsnummer
sed "s/[0-9]*$/*/" Versionsnummer > Zielversionsnummer
Zielvers_VAR=$(cat Zielversionsnummer)
sed  "/$Zielvers_VAR/!d" installiert_vor_Update_am_$datum.txt > Liste_zu_inst_$datum.lst # entfernt alle Zeilen, die nicht "==Versionsnummer" enthalten
echo Ihre neue Versionsnummer ist; echo $Zielvers_VAR
sed -i "s/==.*/==$Zielvers_VAR/" Liste_zu_inst_$datum.lst

echo Inst_Liste_anschauen_02
read

sed  "/$Zielvers_VAR/d" installiert_vor_Update_am_$datum.txt > Liste_pip_Pakete # entfernt alle die "==Versionsnummer" enthalten
sed -i 's/==.*//' Liste_pip_Pakete # Versionsnummer wird entfernt
sed -i 's/^.*.whl$//' Liste_pip_Pakete # whl-Einträge entfernen

echo -e "Sie können jetzt Änderungen an der Datei Liste_zu_inst_$datum.lst im Verzeichnis $pfadtry/$neues_Verzeichnis vornehmen, zum Beispiel Module löschen oder ergänzen. (Weiter mit Eingabetaste.)"
read

## Pakete im VENV updaten ###
pip install --upgrade pip # Pip selbst updaten
pip install -r Liste_pip_Pakete --upgrade # Standard-PIP-Pakete updaten
pip install -r Liste_zu_inst_$datum.lst -U # trytond-Pakete updaten
pip freeze --local > installiert_nach_Update_am_$datum.txt
# rm Zielversionsnummer Versionsnummer Liste_pip_Pakete Liste_zu_inst_$datum.lst

## Globale py-Pakete updaten
deactivate

echo"
Möchten Sie auch die systemweit installierten Python-Pakete updaten (j/n)?
"
read glob
if [ "$glob" = "j" ]
then
pip3 list -o | cut -f1 -d' ' | tr " " "\n" | awk '{if(NR>=3)print}' | cut -d' ' -f1 | xargs -n1 pip3 install -U
fi

echo -e "
Ende des Skripts - wir wünschen noch einen schönen Tag."
