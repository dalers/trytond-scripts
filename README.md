# Tryton installation and maintenance scripts for PIP

A collection of installation and maintenance scripts for Tryton on FreeBSD.

THESE SCRIPTS ARE NOT LIKELY NOT SUITABLE FOR USE AT THIS TIME. USE AT YOUR OWN RISK.

Keep scripts together - there may be inter-dependencies.

Based on https://gitlab.com/TrytonDACH/tryton-installation-and-maintainance-scripts-for-pip

# Sample Databases
## sample databases/
Includes module lists.

# Scripts
## install-X.YYY.sh
Installs trytond from scratch in a virtual environment using PIP. Can install from Tryton project repo or a private repo (using separate module lists).

## upgrade-patch-X.YYY.sh
Upgrade patch - change in z of x.y.z (e.g. 5.6.2 to 5.6.[latest]).

## upgrade-series-X.YYY.sh
Upgrade series - change in x.y of x.y.z (e.g. 5.6.2 > 5.8.[latest]).

## import-database.sh
Imports a database backup into an existing system (does not care about differences in setup or if modules are installed into virtual environment). Can be used to keep development system synchronized with production system (or vice versa).

## start-server-and-client.sh
Start trytond Tryton server daemon and Tryton client.

## backup.sh
Run at system boot or regularly by cron to create a database psql dump. The script compares the dump to the previous dump, if the new dump is the same as the previous dump it is deleted, if not the previous dump is zipped.

## install-modules-X.YYY.sh
Install additional modules into system and database.

# Credits
Thank you to herrdeh (https://discuss.tryton.org/u/herrdeh/summary) for creating the initial scripts, and Eddy Boer (https://discuss.tryton.org/u/edbo/summary) and Dave Harper (https://discuss.tryton.org/u/dave/summary) for their help.
